package com.west.shopviewmodel.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.west.shopviewmodel.model.Item

@Dao
interface ItemDao {

    @Query("select * from Item order by viewCount")
    fun getItems(): LiveData<List<Item>>

    @Query("select * from item where category==:category order by viewCount")
    fun getItemsByCategory(category: String): LiveData<List<Item>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItem(item: Item)
}