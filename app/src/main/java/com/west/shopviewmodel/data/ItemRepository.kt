package com.west.shopviewmodel.data

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import com.west.shopviewmodel.model.Item
import com.west.shopviewmodel.view.FirestroeQueryLiveData

class ItemRepository(application: Application) {
    private var itemDao: ItemDao
    private lateinit var items: LiveData<List<Item>>
    private var isNetwork = false
    private var firestroeQueryLiveData = FirestroeQueryLiveData()

    init {
        itemDao = ItemDatabase.getInstance(application).getItemDao()
        items = itemDao.getItems()
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        isNetwork = networkInfo.isConnected
    }

    fun getAllItems(): LiveData<List<Item>> {
        if (isNetwork) {
            items = firestroeQueryLiveData
        } else {
            items = itemDao.getItems()
        }
        return items
    }

    fun setCategory(categoryId: String) {
        if (isNetwork) {
            firestroeQueryLiveData.setCategory(categoryId)
        } else {
            itemDao.getItemsByCategory(categoryId)
        }
    }
}