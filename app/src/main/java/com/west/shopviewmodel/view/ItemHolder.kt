package com.west.shopviewmodel.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.west.shopviewmodel.R
import com.west.shopviewmodel.model.Item
import kotlinx.android.synthetic.main.item_row.view.*

class ItemHolder(view: View) : RecyclerView.ViewHolder(view) {
    var titleText = view.item_title
    var pirceText = view.item_price
    val image = view.item_image
    var countText = view.item_count
    fun bindTo(item: Item) {
        titleText.text = item.title
        pirceText.text = item.price.toString()
        Glide.with(itemView.context)
            .load(item.imageUrl)
            .apply(RequestOptions().override(120))
            .into(image)
        countText.text = item.viewCount.toString()
        countText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.eye, 0, 0, 0)
    }
}