package com.west.shopviewmodel

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.west.shopviewmodel.data.WatchItem
import com.west.shopviewmodel.model.Item
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class DetailActivity : AppCompatActivity(), AnkoLogger {

    lateinit var item: Item

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        item = intent.getParcelableExtra<Item>("ITEM")
        info("${item.id} / ${item.title}")
        web.settings.javaScriptEnabled = true
        web.loadUrl(item.content)
        val uid = FirebaseAuth.getInstance().currentUser?.uid
        FirebaseFirestore.getInstance().collection("users")
            .document(uid!!)
            .collection("watchItems")
            .document(item.id).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val watchItem = it.result?.toObject(WatchItem::class.java)
                    if (watchItem != null) {
                        watch.isChecked = true
                    }
                }
            }

        watch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                FirebaseFirestore.getInstance().collection("users")
                    .document(uid!!)
                    .collection("watchItems")
                    .document(item.id)
                    .set(WatchItem(item.id))
            } else {
                FirebaseFirestore.getInstance().collection("users")
                    .document(uid!!)
                    .collection("watchItems")
                    .document(item.id)
                    .delete()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        item.viewCount++
        item.id?.let {
            FirebaseFirestore.getInstance().collection("items")
                .document(it).update("viewCount", item.viewCount)
        }
    }
}
