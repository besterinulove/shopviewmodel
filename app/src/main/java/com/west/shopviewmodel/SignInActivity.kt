package com.west.shopviewmodel

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.alert
import org.jetbrains.anko.info
import org.jetbrains.anko.okButton

class SignInActivity : AppCompatActivity(),AnkoLogger {

    private val RC_GOOGLE_SIGN_IN: Int=200
    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        val gso=GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestId()
            .build()
        googleSignInClient=GoogleSignIn.getClient(this,gso)

        google_sign_in.setOnClickListener {
            startActivityForResult(googleSignInClient.signInIntent,RC_GOOGLE_SIGN_IN)
        }
        signup.setOnClickListener {
            signUp()
        }
        login.setOnClickListener {
            login()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==RC_GOOGLE_SIGN_IN){
            val task=GoogleSignIn.getSignedInAccountFromIntent(data)
            val account=task.getResult(ApiException::class.java)
            info("${account?.id}")
            val credential=GoogleAuthProvider.getCredential(account?.idToken,null)
            FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnCompleteListener {task->
                    if(task.isSuccessful){
                        setResult(Activity.RESULT_OK)
                        finish()
                    }else{
                        info("${task.exception?.message}")
                        Snackbar.make(main_signin,"Firebase authentication failed",Snackbar.LENGTH_LONG).show()
                    }
                }
        }
    }

    private fun login() {
        FirebaseAuth.getInstance()
            .signInWithEmailAndPassword(email.text.toString(), password.text.toString())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    alert("${it.exception?.message}", "Login") {
                        okButton { }
                    }.show()
                }
            }
    }

    private fun signUp() {
        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(email.text.toString(), password.text.toString())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    alert("Account created", "Sign Up") {
                        okButton {
                            setResult(Activity.RESULT_OK)
                            finish()
                        }
                    }.show()
                } else {
                    alert("${it.exception?.message}", "Sign Up") {
                        okButton { }
                    }.show()
                }
            }
    }
}
