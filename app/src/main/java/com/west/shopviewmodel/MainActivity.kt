package com.west.shopviewmodel

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.auth.AuthMethodPickerLayout
import com.firebase.ui.auth.AuthUI
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.west.shopviewmodel.model.Category
import com.west.shopviewmodel.model.Item
import com.west.shopviewmodel.view.ItemHolder
import com.west.shopviewmodel.view.ItemViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import java.util.*

class MainActivity : AppCompatActivity(), AnkoLogger, FirebaseAuth.AuthStateListener {


    private lateinit var itemViewModel: ItemViewModel
    private lateinit var adapter: ItemAdapter
    //    private lateinit var adapter: FirestoreRecyclerAdapter<Item, ItemHolder>
    private val RC_SINGIN: Int = 100
    var categories = mutableListOf<Category>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        verify_email.setOnClickListener {
            FirebaseAuth.getInstance().currentUser?.sendEmailVerification()?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Snackbar.make(it, "Verify email sent", Snackbar.LENGTH_LONG).show()
                }
            }
        }

        FirebaseFirestore.getInstance().collection("categories").get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    task.result?.let {
                        categories.add(Category("", "不分類"))
                        for (doc in it) {
                            categories.add(
                                Category(
                                    doc.id,
                                    doc.data.get("name").toString()
                                )
                            )
                        }
                    }
                    spinner.adapter = ArrayAdapter<Category>(
                        this,
                        android.R.layout.simple_spinner_item,
                        categories
                    ).apply {
                        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    }
                    spinner.setSelection(0, false)
                    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                            setupAdapter()
                            itemViewModel.setCategory(categories.get(position).id)
                        }
                    }
                }
            }
        // setRecyclerView
        with(recycler) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        adapter = ItemAdapter(mutableListOf<Item>())
        recycler.adapter = adapter
        itemViewModel = ViewModelProviders.of(this)
            .get(ItemViewModel::class.java)
        itemViewModel.getItems().observe(this, androidx.lifecycle.Observer {
            info("${it.size}")

            adapter.items = it
            adapter.notifyDataSetChanged()
        })
//        setupAdapter()

    }

    inner class ItemAdapter(var items: List<Item>) : RecyclerView.Adapter<ItemHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
            return ItemHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_row, parent, false)
            )
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: ItemHolder, position: Int) {
            holder.bindTo(items.get(position))
            holder.itemView.setOnClickListener {
                itemClick(items.get(position), position)
            }
        }
    }


    private fun itemClick(item: Item, position: Int) {
        info("${item.title} / $position")
        startActivity(intentFor<DetailActivity>("ITEM" to item))
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        val user = auth.currentUser
        info { "${user?.uid}" }
        if (user != null) {
            user_info.text = "Email: ${user.email} / ${user.isEmailVerified}"
//            verify_email.visibility = if (user.isEmailVerified) View.GONE else View.VISIBLE
        } else {
            user_info.text = "Not Login"
            verify_email.visibility = View.GONE
        }
    }

    override fun onStart() {
        super.onStart()
        FirebaseAuth.getInstance().addAuthStateListener(this)
    }

    override fun onStop() {
        super.onStop()
        FirebaseAuth.getInstance().removeAuthStateListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_sinin -> {
                val whiteList = listOf<String>("tw", "hk", "cn", "au")
                val myLayout = AuthMethodPickerLayout.Builder(R.layout.sign_up)
                    .setFacebookButtonId(R.id.bt_fbLogin)
                    .setGoogleButtonId(R.id.bt_googleLogin)
                    .setEmailButtonId(R.id.bt_email)
                    .setPhoneButtonId(R.id.bt_phone)
                    .build()
                startActivityForResult(
                    AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                            Arrays.asList(
                                AuthUI.IdpConfig.EmailBuilder().build(),
                                AuthUI.IdpConfig.GoogleBuilder().build(),
                                AuthUI.IdpConfig.FacebookBuilder().build(),
                                AuthUI.IdpConfig.PhoneBuilder()
                                    .setDefaultCountryIso("tw")
                                    .setWhitelistedCountries(whiteList)
                                    .build()
                            )
                        )
                        .setIsSmartLockEnabled(false)
                        .setLogo(R.drawable.shop)
                        .setTheme(R.style.SignUp)
                        .setAuthMethodPickerLayout(myLayout)
                        .build(),
                    RC_SINGIN
                )
//                startActivityForResult<SignInActivity>(RC_SINGIN)
                true
            }
            R.id.action_signout -> {
                FirebaseAuth.getInstance().signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
